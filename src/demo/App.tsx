import * as React from 'react';
import { ContextMenuProvider } from '../lib';
import { contextMenu } from 'react-contexify';
import { MainMenu } from '../lib';
import '../lib/components/utils/ContextMenu.css';
const ContextMenuData = [
  {
    label: 'visible ',
    visible: true,
    shortcut: '⌘ ⇧ N',
    click: () => {
      console.log('Label 1');
    },
  },
  {
    label: 'Label 2 ',
    visible: true,
    shortcut: '⌘ S',
    click: () => {
      console.log('Label 1');
    },
  },
  {
    label: 'Label 3',
    visible: true,
    click: () => {
      console.log('Label 3');
    },
  },
  {
    label: 'Label 3',
    visible: true,
    click: () => {
      console.log('Label 3');
    },
    submenu: [
      {
        label: 'Sub menu Label 1',
        visible: true,
        click: () => {
          console.log('Sub menu Label 1');
        },
      },
      { type: 'separator' },
      {
        label: 'Sub menu Label 2',
        visible: false,
        click: () => {
          console.log('Sub menu Label 2');
        },
      },
    ],
  },
  {
    label: 'Label 4',
    visible: true,
    click: () => {
      console.log('Label 3');
    },
  },
  {
    label: 'Label 5',
    visible: true,
    click: () => {
      console.log('Label 3');
    },
  },
];

const container = (
  <div style={{ height: '100%', width: '100%' }}>
    jdhbcjsdbjc
    <br />
    sgcjsdbcskdjbc
    <br />
  </div>
);
const menuid = 'thisisunique';

class App extends React.Component<any, any> {
  public render() {
    return (
      <div className={'vskjk'}>
        <ContextMenuProvider menuId={menuid} triggerElement={container} />
        <button
          onContextMenu={e => {
            e.preventDefault();
            contextMenu.show({ id: menuid, event: e });
          }}
        >
          show
        </button>
        <MainMenu menuId={menuid} data={ContextMenuData} />
      </div>
    );
  }
}

export default App;
