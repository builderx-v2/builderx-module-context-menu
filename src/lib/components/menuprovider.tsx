import * as React from "react";
import { MenuProvider } from "react-contexify";
import "react-contexify/dist/ReactContexify.min.css";
import "../components/utils/ContextMenu.css";
type propType = {
  menuId: string;
  triggerElement: React.ReactElement<any>;
};

export default class ContextMenuProvider extends React.Component<
  propType,
  any
> {
  render() {
    return (
      <MenuProvider
        id={this.props.menuId}
        component="span"
        event="onContextMenu"
        storeRef={true}
      >
        {this.props.triggerElement}
      </MenuProvider>
    );
  }
}
