import { Menu } from "react-contexify";
import * as React from "react";
import MenuItems from "./menu-items";
import "react-contexify/dist/ReactContexify.min.css";
import "../components/utils/ContextMenu.css";
type menuType = {
  label?: string;
  click?: () => void;
  visible?: boolean;
  submenu?: menuType[];
  type?: string;
};
type propType = {
  menuId: string;
  data: menuType[];
};

function isVisibleItem(item: menuType) {
  if (item.type !== "separator" && item.visible !== false) {
    if ("submenu" in item) {
      return containsVisibleItems(item.submenu);
    }
    return true;
  }
  return false;
}

function containsVisibleItems(
  data: menuType[] | undefined,
  startIndex: number = 0,
  endIndex?: number
) {
  if (data === undefined) {
    return false;
  }
  if (endIndex === undefined) {
    endIndex = data.length;
  }
  for (var i = startIndex; i < endIndex; i++) {
    var item = data[i];
    if (isVisibleItem(item)) {
      return true;
    }
  }
  return false;
}

export default class MainMenu extends React.Component<propType, any> {
  render() {
    var anyVisible = false;
    this.props.data.forEach(function (item: menuType) {
      if (item.type !== "separator" && item.visible !== false) {
        anyVisible = true;
      }
    });
    if (anyVisible) {
      return (
        <Menu id={this.props.menuId} className="MenuContainer">
          <MenuItems data={this.props.data} key={"menu-container-child"} />
        </Menu>
      );
    } else {
      return null;
    }
  }
}
