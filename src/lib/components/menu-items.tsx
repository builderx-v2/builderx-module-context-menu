import * as React from "react";
import { Submenu, Item } from "react-contexify";
import "react-contexify/dist/ReactContexify.min.css";
import "./utils/ContextMenu.css";

type menuType = {
  label?: string;
  click?: () => void;
  visible?: boolean;
  submenu?: menuType[];
  type?: string;
};
type propType = {
  data: menuType[];
  className?: string;
};

function isVisibleItem(item: menuType) {
  if (item.type !== "separator" && item.visible !== false) {
    if ("submenu" in item) {
      return containsVisibleItems(item.submenu);
    }
    return true;
  }
  return false;
}

function containsVisibleItems(
  data: menuType[] | undefined,
  startIndex?: number,
  endIndex?: number
) {
  if (data === undefined) {
    return false;
  }
  if (startIndex === undefined) {
    startIndex = 0;
  }
  if (endIndex === undefined) {
    endIndex = data.length;
  }
  for (var i = startIndex; i < endIndex; i++) {
    if (isVisibleItem(data[i])) {
      return true;
    }
  }
  return false;
}

export default class MenuItems extends React.PureComponent<propType, any> {
  render() {
    const data = this.props.data;
    let itemBeforeSeparator = false;
    return data.map(function (item: any, i: number) {
      if (
        item.type === "separator" &&
        itemBeforeSeparator &&
        containsVisibleItems(data, i)
      ) {
        itemBeforeSeparator = false;
        return null;
        // return <Separator />;
      } else if (item.type !== "separator" && item.visible !== false) {
        if (item.submenu && containsVisibleItems(item.submenu, 0)) {
          itemBeforeSeparator = true;
          return (
            <Submenu
              label={item.label}
              key={`sub-menu-${i}`}
              arrow={
                <img
                  src={require("./utils/right_arrow.svg")}
                  width={12}
                  height={12}
                  style={{
                    border: "none",
                    marginRight: 0,
                    marginLeft: 0,
                    lineHeight: 0,
                  }}
                />
              }
              disabled={false}
            >
              <MenuItems
                data={item.submenu}
                className="MenuContainer"
                key={`sub-menu-item-${i}`}
              />
            </Submenu>
          );
        } else if (item.submenu) {
          return null;
        } else {
          itemBeforeSeparator = true;
          return (
            <Item
              onClick={() => item.click()}
              disabled={false}
              key={`${item.label}-${i}`}
            >
              <div>{item.label}</div>
              {item.shortcut && (
                <div className="item_right">{item.shortcut}</div>
              )}
            </Item>
          );
        }
      } else {
        return null;
      }
    });
  }
}
