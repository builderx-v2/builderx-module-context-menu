const paths = require("../config/paths");
const fs = require("fs");
const path = require("path");

fs.writeFileSync(
  path.join(paths.appBuild, "index.d.ts"),
  `import * as index from "./lib";
export default index;
`
);
