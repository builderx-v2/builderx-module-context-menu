import * as React from "react";
import "react-contexify/dist/ReactContexify.min.css";
import "./utils/ContextMenu.css";
declare type menuType = {
    label?: string;
    click?: () => void;
    visible?: boolean;
    submenu?: menuType[];
    type?: string;
};
declare type propType = {
    data: menuType[];
    className?: string;
};
export default class MenuItems extends React.PureComponent<propType, any> {
    render(): (JSX.Element | null)[];
}
export {};
