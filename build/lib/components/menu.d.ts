import * as React from "react";
import "react-contexify/dist/ReactContexify.min.css";
import "../components/utils/ContextMenu.css";
declare type menuType = {
    label?: string;
    click?: () => void;
    visible?: boolean;
    submenu?: menuType[];
    type?: string;
};
declare type propType = {
    menuId: string;
    data: menuType[];
};
export default class MainMenu extends React.Component<propType, any> {
    render(): JSX.Element | null;
}
export {};
