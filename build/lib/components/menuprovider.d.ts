import * as React from "react";
import "react-contexify/dist/ReactContexify.min.css";
import "../components/utils/ContextMenu.css";
declare type propType = {
    menuId: string;
    triggerElement: React.ReactElement<any>;
};
export default class ContextMenuProvider extends React.Component<propType, any> {
    render(): JSX.Element;
}
export {};
